import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-tabla2',
  templateUrl: './tabla2.component.html',
  styleUrls: ['./tabla2.component.css']
})
export class Tabla2Component implements OnInit {

  
   ListaT:string[]=[];
   list:string[]=[];
   form!:FormGroup;
   valid:boolean=true;
   constructor(private _snackbar:MatSnackBar, private fb:FormBuilder ) {
     this.cargarForm1();
     this.cargarListaT();
 
    }
 
    get getListaform1(){
     return this.form.get('listatexto') as FormArray;
   }
 
   ngOnInit(): void {
   }
 
 
   //Formulario uno------------------------
   
 
   cargarForm1(){
     this.form = this.fb.group({
       listatexto:this.fb.array([])
     });
 
     this.getListaform1.push(this.fb.group({
       texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
       buttonValue : [null,Validators.required]
     }),
     );
     this.getListaform1.push(this.fb.group({
       texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
       buttonValue : [null,Validators.required]
     }),
     );
     this.getListaform1.push(this.fb.group({
       texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
       buttonValue : [null,Validators.required]
     }),
     );
     this.getListaform1.push(this.fb.group({
       texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
       buttonValue : [null,Validators.required]
     }),
     );
     this.getListaform1.push(this.fb.group({
       texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
       buttonValue : [null,Validators.required]
     }),

     );
     this.getListaform1.push(this.fb.group({
       texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
       buttonValue : [null,Validators.required]
     }),
     );
     
   }
   
   darValor(event:any,id:number){
     if (this.getListaform1.at(id).get('texto')?.valid) {
       if(event.keyCode==13){
        
         this.getListaform1.at(id).get('buttonValue')?.setValue(this.getListaform1.at(id).get('texto')?.value);
       }
     }
   }
 
   guardar2(){
     this.cargarLista() 
     //console.log(this.list);
      
   }
 
   //Formulario segundo___________________
   
   cargarLista(){
     
     for (let i = 0; i < this.getListaform1.length; i++) {
         if (this.getListaform1.at(i).get('buttonValue')?.valid) {
          // console.log(this.getListaform1.at(i).get('buttonValue')?.value == null);
           
           this.list.push(this.getListaform1.at(i).get('buttonValue')?.value);
         }
        }
 
   }
 
 
   eliminar(id:number){
     const opcion = confirm('Estas seguro de eliminar el texto');
        if (opcion) {
         // this.list =this.list.filter(data => {
         //   return data!==id; 
         // })
     
         this.list =this.list.filter((data,i) => {
           return i!=id;
         })
        this._snackbar.open('El usuario fue eliminado con exito', '', {
          duration: 1500,
          horizontalPosition: 'center',
          verticalPosition: 'bottom',
        });
      }
   }
 
   anadirT(){
     // this._listaService.agregarALista();
     // this._listaService.limpiarListaPrevia();
     // this.cargarLista();
     this.cargarListaT()
    }
    
    cargarListaT(){
      this.list.forEach(e =>{
        this.ListaT.push(e)
      });
    }
  
   clear(){
     this.list=[];
     // this.cargarLista();
   }
 
   //Formulario tres:--------------------------------------
   
 
 }