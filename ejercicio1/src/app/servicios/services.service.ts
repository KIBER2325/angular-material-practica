import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

import { HttpClient } from '@angular/common/http';
import {  ValoresI } from 'src/app/interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(public http: HttpClient) { }

  getInfo(): Observable<ValoresI[]>{
    return this.http.get<ValoresI[]>('./assets/data.json')
  }
} 




