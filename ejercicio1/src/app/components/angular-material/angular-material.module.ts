import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import {MatButtonModule} from '@angular/material/button';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';


import {MatSelectModule} from '@angular/material/select'; 

import { FormsModule, ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatTreeModule,
    MatIconModule,
    MatSelectModule,
   ReactiveFormsModule,
   FormsModule
   
  ],
  exports:[
    MatSnackBarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatTreeModule,
    MatIconModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule
    
  ]
})
export class AngularMaterialModule { }
