import { NestedTreeControl} from '@angular/cdk/tree';
import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {  MatTreeNestedDataSource} from '@angular/material/tree';

import {  ValoresI } from 'src/app/interfaces/interface';
import { ServicesService } from 'src/app/servicios/services.service';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})

export class FormularioComponent implements OnInit {

  form!: FormGroup
  valor1: string = ''
  number!: number;
  Valuenivel: number = 0;

  dataSource = new MatTreeNestedDataSource<ValoresI>()
  treeControl = new NestedTreeControl<ValoresI>(node => node.children)


  constructor(private fb: FormBuilder,
              private _Service: ServicesService) {
    this.form = fb.group({
     
      niveles: [1, [Validators.min(1), Validators.max(100), Validators.required]],
      repeticiones: [1, [Validators.min(1), Validators.max(100), Validators.required]],
    valor1: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
    valor2: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
    valor3: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
    valor4: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]]
    })
  }

  ngOnInit(): void {
  }

  generarDatos() {
    let {niveles, repeticiones, valor1, valor2, valor3, valor4} = this.form.value
    var values =[valor1, valor2, valor3, valor4]
    var result:ValoresI[] = []
    var primeraVez = true

    

    while (niveles > 0) {
      let value = this.getValue(niveles,values)

        result = this.anidacion(result, repeticiones, value)
      
      niveles--
    }
   
    this.dataSource.data = result;
  }
  agregar() {
   
    this.form.valid? this.generarDatos(): ""
  }
 
  
  hasChild = (_: number, node: ValoresI) => !!node.children && node.children.length > 0;


  getValue(num: number, values: string[]): string {
    if (num > 4) {
      return this.getValue(num-4, values)
    } else {
      return values[num-1]
    }
  }

  anidacion(res:ValoresI[], rep:number, value:string): ValoresI[] {
    var listHelp: ValoresI[] = []

    for (let i = 0; i < rep; i++) {
      let pattern: ValoresI={
        name: value,
        children: res
      }
      listHelp.push(pattern)
    }
    return listHelp
  }

  guardar(): void{
    console.log('guardar');
    
  console.log(this.form.value);
  this.Valuenivel = this.form.value.Nvel
  
 this.valor1 = this.form.value.valor1

  }

}