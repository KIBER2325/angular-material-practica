import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formcheck',
  templateUrl: './formcheck.component.html',
  styleUrls: ['./formcheck.component.css']
})
export class FormcheckComponent implements OnInit {

  
  guardados:string[]=[];
  form!:FormGroup;
  contador:number= 0
  
  constructor(private fb:FormBuilder) { 
    this.crearForm();
  }

  get obtenerNombre(){
    
    return this.form.get('inputName');
  }

  get getNamesCheck(){
    return this.form.get('checkName') as FormArray;
  }


  ngOnInit(): void {
  }

  crearForm(){
    this.form = this.fb.group({
      inputName: ['',[Validators.required,Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      
      checkName:this.fb.array([]), 
    })

   
  }

  guardartodos():void{
   
    this.guardados=[];

    for (let i = 0; i < this.getNamesCheck.length; i++) {
      
      if (this.getNamesCheck.at(i).get('check')?.value ==true )
       {
         this.guardados.push( this.getNamesCheck.at(i).get('nombre')?.value);
        }  
    }
    console.log(this.guardados);
  }



  limpiarCheckboxes():void{
    
    this.getNamesCheck.clear();
  }

  clearBox():void{
    this.guardados=[''];
  }

  adicionar():void{
   
    if (this.obtenerNombre?.valid) {
      
      const nuevo = this.fb.group({
        check: [null],
        nombre : [this.form.get('inputName')?.value]
        
      });

      console.log(nuevo.get('nombre')?.value);
      
      this.getNamesCheck.push(nuevo);
    } else {
      return;
    }
  }

 mostrar():string{
      
      return this.guardados.join("\n");
  }

}
