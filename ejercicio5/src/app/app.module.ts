import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormcheckComponent } from './components/formcheck/formcheck.component';
import { libreriasAngularMateial } from './shared/sharet';


@NgModule({
  declarations: [
    AppComponent,
    FormcheckComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    libreriasAngularMateial
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
