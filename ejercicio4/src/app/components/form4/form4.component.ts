import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form4',
  templateUrl: './form4.component.html',
  styleUrls: ['./form4.component.css']
})
export class Form4Component implements OnInit {
  form!:FormGroup;
  valores: string[]=[]

  constructor(private fb : FormBuilder){
      this.crearCaja();
   }
   crearCaja():void{
     this.form = this.fb.group({
      caja2:[''],
      caja1: this.fb.array([[]])
       
     })
   }
      
  ngOnInit(): void {
  }
  
  get valoresCaja(){
    return this.form.get('caja1') as FormArray
  }

guardar():void{
  this.valores = this.form.value.caja1.join('\n');
 
}

adicionar():void{
  this.valoresCaja.push(this.fb.control('',  [Validators.required , Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]))
}

eliminarCajita(i:number):void{
  this.valoresCaja.removeAt(i)
}





limpiarCaja2():void{
  this.valores = ['']
  
}

limpiarAmbas():void{

  this.valores = [''],
  this.form.reset
 }


}
