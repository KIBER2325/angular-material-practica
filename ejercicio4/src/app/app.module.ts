import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Form4Component } from './components/form4/form4.component';
import { angularmaterial } from './components/shared/shared';
@NgModule({
  declarations: [
    AppComponent,
    Form4Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    angularmaterial
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
